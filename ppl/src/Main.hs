{-# language OverloadedStrings #-}

import MyPlot

import qualified Graphics.Vega.VegaLite as VL

import Control.Monad.Bayes.Class
import Control.Monad.Bayes.Sampler
import Control.Monad.Bayes.Traced
import Control.Monad.Bayes.Weighted

model3 :: MonadInfer m => m (Double, Double)
model3 = do
    b <- uniform (-1) 1
    m <- uniform (-1) 1
    condition $ (b-m) > 0
    return (b, m)

main :: IO ()
main = do
    let nsamples = 5000
    modelsamples <- sampleIOfixed $ prior $ mh nsamples model3
    let (xValues, yValues) = unzip modelsamples
    VL.toHtmlFile "test.html"  $ plot (600, 300)
                  [density2DPlot "b" "m" (-1.1,1.1) (-1.1,1.1)]
                  [("b", VL.Numbers xValues), ("m", VL.Numbers yValues)]
    
