import Control.Monad (unless)

import qualified OpenCV as CV

main :: IO ()
main = do
    cap <- CV.newVideoCapture
    CV.exceptErrorIO $ CV.videoCaptureOpen cap $ CV.VideoDeviceSource 0 Nothing
    isOpened <- CV.videoCaptureIsOpened cap
    case isOpened of
        False -> putStrLn "Couldn't open video capture device"
        True -> CV.withWindow "my video app" $ loop cap
    CV.exceptErrorIO $ CV.videoCaptureRelease cap

loop :: CV.VideoCapture -> CV.Window -> IO ()
loop cap window = do
    _ <- CV.videoCaptureGrab cap
    frame <- CV.videoCaptureRetrieve cap
    case frame of
        Nothing -> return ()
        Just img -> do
            CV.imshow window img
            key <- CV.waitKey 10
            unless (key == 27) $ loop cap window


