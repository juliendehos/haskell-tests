{-# language DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Control.Monad (unless)
import Data.Int (Int32)
import GHC.Word (Word8)
import Linear.V2 (V2(..))
import OpenCV
import OpenCV.VideoIO.Types
import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    if length args /= 1
    then putStrLn "Specify an input video file, please"
    else do
        let infile = head args
        cap <- newVideoCapture
        exceptErrorIO $ videoCaptureOpen cap $ VideoFileSource infile Nothing
        isOpened <- videoCaptureIsOpened cap
        case isOpened of
            False -> putStrLn $ "Couldn't open " ++ infile
            True -> do
                fps <- videoCaptureGetD cap VideoCapPropFps
                let waitTime = round $ 0.7 * 1000.0 / fps
                withWindow "my video app" $ loop cap waitTime

loop :: VideoCapture -> Int32 -> Window -> IO ()
loop cap waitTime window = do
    _ <- videoCaptureGrab cap
    frame <- videoCaptureRetrieve cap
    case frame of
        Nothing -> return ()
        Just img -> do
            imshow window $ process img
            key <- waitKey waitTime
            unless (key == 27) $ loop cap waitTime window

process :: Mat ('S ['D, 'D]) 'D 'D 
        -> Mat ('S ['D, 'D]) ('S 1) ('S Word8)
process img = exceptError $ do
    img1 :: (Mat ('S ['D, 'D]) ('S 3) ('S Word8)) <- coerceMat img
    img2 <- gaussianBlur (V2 9 9) 1.5 1.5 img1
    canny 0 40 (Just 3) CannyNormL1 img2

