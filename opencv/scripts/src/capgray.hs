{-# language DataKinds #-}

import Control.Monad (unless)
import GHC.Word (Word8)
import OpenCV
import OpenCV.Unsafe

main :: IO ()
main = do
    cap <- newVideoCapture
    exceptErrorIO $ videoCaptureOpen cap $ VideoDeviceSource 0 Nothing
    isOpened <- videoCaptureIsOpened cap
    case isOpened of
        False -> putStrLn "Couldn't open video capture device"
        True -> withWindow "my video app" $ loop cap
    exceptErrorIO $ videoCaptureRelease cap

loop :: VideoCapture -> Window -> IO ()
loop cap window = do
    _ <- videoCaptureGrab cap
    frame <- videoCaptureRetrieve cap
    case frame of
        Nothing -> return ()
        Just img -> do
            let img' :: Mat ('S ['D, 'D]) ('S 3) ('S Word8)
                img' = exceptError $ coerceMat img
            let grayImg = exceptError $ cvtColor bgr gray img'
            imshow window $ unsafeCoerceMat grayImg
            key <- waitKey 10
            unless (key == 27) $ loop cap window

