{-# language DataKinds #-}

import Control.Concurrent 
import Control.Monad 
import OpenCV
import OpenCV.VideoIO.Types

main :: IO ()
main = do
    capMaybe <- openCam
    case capMaybe of
        Nothing -> putStrLn "couldn't open cam device"
        Just cap -> do
            imgVar <- newEmptyMVar
            _ <- forkIO $ runCam cap imgVar
            withWindow "my video app" $ runDisplay imgVar
            closeCam cap

runCam :: VideoCapture -> MVar (Mat ('S ['D, 'D]) 'D 'D) -> IO ()
runCam cap imgVar = forever $ do
    putStrLn "runCam"
    imgMaybe <- captureCam cap
    case imgMaybe of
        Nothing -> return ()
        Just img -> putMVar imgVar img

runDisplay :: MVar (Mat ('S ['D, 'D]) 'D 'D) -> Window -> IO ()
runDisplay imgVar window = do
    putStrLn "runDisplay"
    img <- takeMVar imgVar
    imshow window img
    key <- waitKey 30
    -- key <- waitKey 500
    unless (key == 27) $ runDisplay imgVar window

-- camera

openCam :: IO (Maybe VideoCapture)
openCam = do
    cap <- newVideoCapture
    exceptErrorIO $ videoCaptureOpen cap $ VideoDeviceSource 0 Nothing
    isOpened <- videoCaptureIsOpened cap
    case isOpened of
        False -> return Nothing
        True -> videoCaptureSetD cap VideoCapPropFps 5 >> (return $ Just cap)

captureCam :: VideoCapture -> IO (Maybe (Mat ('S ['D, 'D]) 'D 'D))
captureCam cap = videoCaptureGrab cap >> videoCaptureRetrieve cap

closeCam :: VideoCapture -> IO ()
closeCam cap = exceptErrorIO $ videoCaptureRelease cap


