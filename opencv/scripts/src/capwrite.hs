import qualified Data.ByteString as B
import OpenCV

main :: IO ()
main = do
    cap <- newVideoCapture
    exceptErrorIO $ videoCaptureOpen cap $ VideoDeviceSource 0 Nothing
    isOpened <- videoCaptureIsOpened cap
    case isOpened of
        False -> putStrLn "Couldn't open video capture device"
        True -> do
            _ <- videoCaptureGrab cap
            frame <- videoCaptureRetrieve cap
            case frame of
                Nothing -> return ()
                Just img -> do
                    let stream = exceptError $ imencode (OutputPng defaultPngParams) img
                    B.writeFile "out.png" stream
    exceptErrorIO $ videoCaptureRelease cap

