{-# language DataKinds #-}

import Control.Monad (unless)
import Data.Int (Int32)
import OpenCV
import OpenCV.TypeLevel
import OpenCV.VideoIO.Types
import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    if length args /= 1
    then putStrLn "Specify an input video file, please"
    else do
        let infile = head args
        cap <- newVideoCapture
        exceptErrorIO $ videoCaptureOpen cap $ VideoFileSource infile Nothing
        isOpened <- videoCaptureIsOpened cap
        case isOpened of
            False -> putStrLn $ "Couldn't open " ++ infile
            True -> do
                fps <- videoCaptureGetD cap VideoCapPropFps
                let waitTime = round $ 0.7 * 1000.0 / fps
                withWindow "my video app" $ loop cap waitTime

loop :: VideoCapture -> Int32 -> Window -> IO ()
loop cap waitTime window = do
    _ <- videoCaptureGrab cap
    frame <- videoCaptureRetrieve cap
    case frame of
        Nothing -> return ()
        Just img -> do
            imshow window $ process img
            key <- waitKey waitTime
            unless (key == 27) $ loop cap waitTime window

process :: Mat ('S ['D, 'D]) 'D 'D
        -> Mat ('S ['D, 'D]) 'D 'D
process img = exceptError $ do
    img1 <- cvtColor bgr gray img
    img2 <- cvtColor gray bgr img1
    coerceMat img2


