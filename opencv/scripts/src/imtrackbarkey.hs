{-# language DataKinds #-}

import Control.Concurrent (MVar, newMVar, modifyMVar_, readMVar)
import Control.Monad (unless)
import Control.Monad.Trans (liftIO)
import qualified Data.ByteString as B
import Linear.V4 ( V4(..)  )
import System.Environment (getArgs)

import OpenCV

data Params = Params
    { pMul :: Double
    , pAdd :: Double
    }

main :: IO ()
main = do
    args <- getArgs
    if null args
    then putStrLn "specify an image file, please"
    else do
        let filename = head args
        params <- newMVar (Params 1 0)
        img <- imdecode ImreadAnyColor <$> B.readFile filename
        withWindow "test" $ \w -> do
            createTrackbar w "pMul" 100 100 $ \c -> 
                modifyMVar_ params (\ p -> 
                    return $ Params (fromIntegral c / 100) (pAdd p))
            createTrackbar w "pAdd" 0 255 $ \c -> 
                modifyMVar_ params (\ p -> 
                    return $ Params (pMul p) (fromIntegral c))
            loop img params w


loop :: Mat ('S ['D, 'D]) ('D) 'D -> MVar Params -> Window -> IO ()
loop img params window = do
    img' <- process img params
    imshow window img'
    key <- waitKey 10
    unless (key == 27) $ loop img params window

process :: Mat ('S ['D, 'D]) ('D) 'D -> MVar Params -> IO (Mat ('S ['D, 'D]) ('D) 'D)
process img params = do
    (Params pm pa) <- readMVar params
    let img1 = matScalarMult img pm
    return $ matScalarAdd img1 (V4 pa pa pa pa)

