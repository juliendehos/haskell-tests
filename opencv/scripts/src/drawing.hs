{-# LANGUAGE DataKinds #-}

import Data.Proxy (Proxy(..))
import Data.Int (Int32)
import Data.Word (Word8)
import Control.Monad (forever, when)
import Control.Monad.Trans.Class (lift)
import Linear.V4 (V4(..))
import Linear.V2 (V2(..))
import OpenCV as CV
import System.Exit (exitSuccess)

white, black :: Scalar
white = toScalar (V4 255 255 255 255 :: V4 Double)
black = toScalar (V4   0   0   0 255 :: V4 Double)

myExampleImg :: Mat (ShapeT [280, 280]) ('S 4) ('S Word8)
myExampleImg = CV.exceptError $ withMatM
    (Proxy :: Proxy [280, 280])
    (Proxy :: Proxy 4)
    (Proxy :: Proxy Word8)
    black $ \imgM -> 
        lift $ circle imgM (V2 100 100 :: V2 Int32) 70 white 5 LineType_AA 0

main :: IO ()
main = CV.withWindow "mkmat" $ \window -> do
    CV.imshow window myExampleImg
    CV.resizeWindow window 280 280
    forever $ do
        key <- CV.waitKey 0
        when (key == 27) exitSuccess

