{-# language DataKinds #-}

import Control.Concurrent 
import Control.Monad 
import Data.IORef
import OpenCV
import OpenCV.VideoIO.Types

main :: IO ()
main = do
    capMaybe <- openCam
    case capMaybe of
        Nothing -> putStrLn "couldn't open cam device"
        Just cap -> do
            Just img0 <- captureCam cap
            imgRef <- newIORef img0
            _ <- forkIO $ runCam cap imgRef
            withWindow "my video app" $ runDisplay imgRef
            closeCam cap

runCam :: VideoCapture -> IORef (Mat ('S ['D, 'D]) 'D 'D) -> IO ()
runCam cap imgRef = forever $ do
    putStrLn "runCam"
    imgMaybe <- captureCam cap
    case imgMaybe of
        Nothing -> return ()
        Just img -> atomicWriteIORef imgRef img

runDisplay :: IORef (Mat ('S ['D, 'D]) 'D 'D) -> Window -> IO ()
runDisplay imgRef window = do
    putStrLn "runDisplay"
    img <- readIORef imgRef
    imshow window img
    key <- waitKey 30
    -- key <- waitKey 1000
    unless (key == 27) $ runDisplay imgRef window

-- camera

openCam :: IO (Maybe VideoCapture)
openCam = do
    cap <- newVideoCapture
    exceptErrorIO $ videoCaptureOpen cap $ VideoDeviceSource 0 Nothing
    isOpened <- videoCaptureIsOpened cap
    case isOpened of
        False -> return Nothing
        True -> videoCaptureSetD cap VideoCapPropFps 5 >> (return $ Just cap)

captureCam :: VideoCapture -> IO (Maybe (Mat ('S ['D, 'D]) 'D 'D))
captureCam cap = videoCaptureGrab cap >> videoCaptureRetrieve cap

closeCam :: VideoCapture -> IO ()
closeCam cap = exceptErrorIO $ videoCaptureRelease cap

