{-# language DataKinds #-}

import Control.Monad (unless)
import OpenCV.TypeLevel
import OpenCV.VideoIO.Types

import qualified OpenCV as CV

main :: IO ()
main = do
    cap <- CV.newVideoCapture
    CV.exceptErrorIO $ CV.videoCaptureOpen cap $ CV.VideoDeviceSource 0 Nothing
    isOpened <- CV.videoCaptureIsOpened cap
    case isOpened of
        False -> putStrLn "Couldn't open video capture device"
        True -> do
            CV.videoCaptureSetD cap VideoCapPropFps 5
            w <- CV.videoCaptureGetI cap VideoCapPropFrameWidth
            h <- CV.videoCaptureGetI cap VideoCapPropFrameHeight
            f <- CV.videoCaptureGetD cap VideoCapPropFps
            putStrLn $ "Video size: " ++ show w ++ " x " ++ show h
            putStrLn $ "Frame rate: " ++ show f
            CV.withWindow "my video app" $ loop cap
    CV.exceptErrorIO $ CV.videoCaptureRelease cap

loop :: CV.VideoCapture -> CV.Window -> IO ()
loop cap window = do
    _ <- CV.videoCaptureGrab cap
    frame <- CV.videoCaptureRetrieve cap
    case frame of
        Nothing -> return ()
        Just img -> do
            let img' :: CV.Mat ('S ['D, 'D]) 'D 'D
                img' = CV.exceptError $ CV.coerceMat img
            CV.imshow window img'
            key <- CV.waitKey 20
            unless (key == 27) $ loop cap window

