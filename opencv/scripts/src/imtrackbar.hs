{-# language DataKinds #-}

import Control.Monad (void)
import qualified Data.ByteString as B
import System.Environment (getArgs)

import OpenCV

main :: IO ()
main = do
    args <- getArgs
    if null args
    then putStrLn "specify an image file, please"
    else do
        let filename = head args
        img <- imdecode ImreadAnyColor <$> B.readFile filename
        withWindow "test" $ \window -> do
            imshow window img
            createTrackbar window "coef" 100 100 $ \c -> do
                let k = (fromIntegral c) / 100.0
                    img' = matScalarMult img k
                imshow window img'
            void $ waitKey 0

