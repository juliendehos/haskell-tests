
let

  config = {
    packageOverrides = pkgs: {

      opencv3 = (pkgs.opencv3.override {
        enableContrib = true;
        enableCuda = false;
        enableEXR = false;
        enableFfmpeg = true;
        enableGtk3 = true;
        enableJPEG2K = false;
        enableTIFF = false;
        enableWebP = false;
      }).overrideDerivation (attrs: {
        doCheck = false;
      });

      haskellPackages = pkgs.haskellPackages.override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {
          opencv = pkgs.haskell.lib.dontCheck haskellPackagesOld.opencv;
        };
      };

    };
  };

  #rev = "a91c293aaf2f6b2939418e3be9f0d2b1b349e43c";
  rev = "18.03";
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz") { 
    config = config;
  };

in

(pkgs.haskellPackages.mkDerivation {
  pname = "myscripts";
  version = "0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = with pkgs.haskellPackages; [
    base
    bytestring
    linear
    opencv
    opencv-extra
  ];
  license = pkgs.stdenv.lib.licenses.mit;
}).env

