import cv2 as cv
import numpy as np
import tensorflow.keras as kr

WIDTH, HEIGHT = 280, 280
WINDOW_NAME = "mnist_gui"
BRUSH_RADIUS = 20
BRUSH_COLOR = (255, 255, 255)
BACKGROUND_COLOR = 0

model = kr.models.load_model('mnist_model.h5')


def predict(img):
    ml_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    ml_img = cv.resize(ml_img, (28, 28))
    ml_img = ml_img.astype('float32')
    ml_img /= 255
    ml_img = ml_img.reshape(1, 28, 28, 1)
    prediction = model.predict_classes(ml_img)
    print(prediction[0])


class Params:
    def __init__(self):
        self.p0 = (-1, -1)
        self.img = np.empty((WIDTH, HEIGHT, 3), 'float32')
        self.img.fill(BACKGROUND_COLOR)


def drawing_callback(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        params.p0 = (x, y)
    elif event == cv.EVENT_MOUSEMOVE:
        if flags & cv.EVENT_FLAG_LBUTTON:
            p1 = (x, y)
            cv.line(params.img, params.p0, p1, BRUSH_COLOR, BRUSH_RADIUS)
            params.p0 = p1
    elif event == cv.EVENT_LBUTTONUP:
        predict(params.img)
    elif event == cv.EVENT_RBUTTONUP:
        params.img.fill(BACKGROUND_COLOR)


params = Params()
cv.namedWindow(WINDOW_NAME)
cv.setMouseCallback(WINDOW_NAME, drawing_callback, params)

print('''
Draw a digit in the window:
    - left click -> draw
    - right click -> clear image
    - esc -> quit
''')

while True:
    cv.resizeWindow(WINDOW_NAME, WIDTH, HEIGHT)
    cv.imshow(WINDOW_NAME, params.img)
    key = cv.waitKey(10)
    if key == 27:    # esc
        break
