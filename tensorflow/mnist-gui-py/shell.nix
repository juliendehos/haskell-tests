with (import <nixpkgs> {}).python3Packages;

let

  _opencv3 = opencv3.override {
    enableGtk3 = true;

    enableTIFF = false;
    enableWebP = false;
    enableEXR = false;
    enableJPEG2K = false;
    enableContrib = false;
  };

in

buildPythonPackage {
  name = "mnist_gui_py";
  src = ./.;
  buildInputs = [ 
    _opencv3

    h5py
    scipy
    tensorflow
  ];
}

