# https://github.com/Gabriel439/haskell-nix/blob/master/project2/README.md

let

  config = {
    packageOverrides = pkgs: rec {

      protobuf = pkgs.protobuf3_5;

      # tensorflow = (pkgs.tensorflow.override {
      #   protobuf = pkgs.protobuf3_5;
      # }).overrideDerivation (attrs: {
      #   doCheck = false;
      # });

      _opencv3 = (pkgs.opencv3.override {
        enableGtk2 = true;
        enableContrib = false;
        enableTIFF = false;
        enableWebP = false;
        enableEXR = false;
        enableJPEG2K = false;
        enableEigen = false;
        enableOpenblas = false;

        protobuf = pkgs.protobuf3_5;
      }).overrideDerivation (attrs: {
        doCheck = false;
      });

      opencv3 = pkgs.callPackage ./opencv3.nix {
        protobuf = pkgs.protobuf3_5;
      };

      haskellPackages = pkgs.haskellPackages.override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {

          opencv = pkgs.haskell.lib.dontCheck (
            haskellPackagesNew.callPackage ./opencvhs.nix {
              opencv = opencv3;
              #protobuf = pkgs.protobuf3_5;
            });

          };
        };

      };
    };

  rev = "a91c293aaf2f6b2939418e3be9f0d2b1b349e43c";
  url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  pkgs = import (fetchTarball url) { inherit config; };
  
  drv = pkgs.haskellPackages.callCabal2nix "mnistguihs" ./. {};

in

if pkgs.lib.inNixShell then drv.env else drv

