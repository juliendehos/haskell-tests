{-# LANGUAGE DataKinds #-}

import Data.Proxy (Proxy(..))
import Data.Int (Int32)
import Data.Word (Word8)
import Control.Monad (forever, when)
import Control.Monad.Trans.Class (lift)
import Linear.V4 (V4(..))
import Linear.V2 (V2(..))
import OpenCV as CV
import System.Exit (exitSuccess)
import qualified TensorFlow.Core as TF

-- TODO dnn prediction
-- width, height

brush :: Int32
brush = 20

white, black :: Scalar
white = toScalar (V4 255 255 255 255 :: V4 Double)
black = toScalar (V4   0   0   0 255 :: V4 Double)

type Img = Mat (ShapeT [280, 280]) ('S 4) ('S Word8)

blackImg :: Img
blackImg = CV.exceptError $ mkMat
    (Proxy :: Proxy [280, 280])
    (Proxy :: Proxy 4)
    (Proxy :: Proxy Word8)
    black 

lineImg :: Img -> (Int32, Int32) -> (Int32, Int32) -> Img
lineImg img0 (x0, y0) (x1, y1) = CV.exceptError $ withMatM
    (Proxy :: Proxy [280, 280])
    (Proxy :: Proxy 4)
    (Proxy :: Proxy Word8)
    black $ \imgM -> do
        matCopyToM imgM (V2 0 0) img0 Nothing
        lift $ line imgM (V2 x0 y0) (V2 x1 y1) white brush LineType_AA 0

handleMouse :: Window -> Img -> (Int32, Int32) -> MouseCallback
handleMouse win img (x0, y0) evt x1 y1 flags = case evt of 
    EventLButtonDown -> 
        setMouseCallback win (handleMouse win img (x1, y1))
    EventMouseMove -> when (hasLButton flags) $ do
        let img' = lineImg img (x0, y0) (x1, y1)
        CV.imshow win img'
        setMouseCallback win (handleMouse win img' (x1, y1))
    EventRButtonUp -> do
        CV.imshow win blackImg
        setMouseCallback win (handleMouse win blackImg (0, 0))
    _ -> return ()

main :: IO ()
main = do
    putStrLn "Draw a digit in the window:"
    putStrLn "    - left click -> draw"
    putStrLn "    - right click -> clear image"
    putStrLn "    - esc -> quit"
    withWindow "gui drawing" $ \win -> do
        resizeWindow win 280 280
        CV.imshow win blackImg
        setMouseCallback win (handleMouse win blackImg (0, 0))
        forever $ do
            key <- CV.waitKey 0
            when (key == 27) exitSuccess

