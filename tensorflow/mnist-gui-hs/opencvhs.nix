{ mkDerivation, aeson, base, base64-bytestring, bindings-DSL
, bytestring, Cabal, containers, criterion, data-default, deepseq
, directory, fetchgit, Glob, haskell-src-exts, inline-c
, inline-c-cpp, JuicyPixels, lens, linear, mtl, opencv, primitive
, QuickCheck, random, repa, stdenv, tasty, tasty-hunit
, tasty-quickcheck, template-haskell, text, transformers, vector
, protobuf
}:
mkDerivation {
  pname = "opencv";
  version = "0.0.2.1";
  src = fetchgit {
    url = "git://github.com/LumiGuide/haskell-opencv";
    rev = "dd51d7cfb1513e03516706465c246164e33fb589";
    sha256 = "09p5lbm8x6lw4mb7aaa070lx34g0fpj46d3zflk8aksn4h8b5w2l";
  };
  postUnpack = "sourceRoot+=/opencv; echo source root reset to $sourceRoot";
  setupHaskellDepends = [ base Cabal ];
  libraryHaskellDepends = [
    aeson base base64-bytestring bindings-DSL bytestring containers
    data-default deepseq inline-c inline-c-cpp JuicyPixels linear mtl
    primitive repa template-haskell text transformers vector
  ];
  #libraryPkgconfigDepends = [ opencv protobuf ];
  libraryPkgconfigDepends = [ opencv ];
  testHaskellDepends = [
    base bytestring containers data-default deepseq directory Glob
    haskell-src-exts JuicyPixels lens linear primitive QuickCheck
    random repa tasty tasty-hunit tasty-quickcheck template-haskell
    text transformers vector
  ];
  benchmarkHaskellDepends = [
    base bytestring criterion linear repa vector
  ];
  hardeningDisable = [ "bindnow" ];
  homepage = "https://github.com/LumiGuide/haskell-opencv";
  description = "Haskell binding to OpenCV-3.x";
  license = stdenv.lib.licenses.bsd3;
}

