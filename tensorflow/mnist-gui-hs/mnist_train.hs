{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad (forM_, when)
import Control.Monad.IO.Class (liftIO)
import Data.Int (Int32, Int64)
import Data.List (genericLength)
import qualified Data.Vector as V
import Text.Printf

import qualified TensorFlow.Core as TF
import qualified TensorFlow.Minimize as TF
import qualified TensorFlow.Ops as TF hiding (initializedVariable, zeroInitializedVariable)
import qualified TensorFlow.Variable as TF

import TensorFlow.Examples.MNIST.InputData
import TensorFlow.Examples.MNIST.Parse

batchSize = 64
epochs = 3
numPixels = 28*28
numLabels = 10

type LabelType = Int32

data Model = Model
    { train :: TF.TensorData Float -> TF.TensorData LabelType -> TF.Session ()
    , infer :: TF.TensorData Float -> TF.Session (V.Vector LabelType)
    , errorRate :: TF.TensorData Float -> TF.TensorData LabelType -> TF.Session Float
    }

-- Create tensor with random values where the stddev depends on the width.
randomParam :: Int64 -> TF.Shape -> TF.Build (TF.Tensor TF.Build Float)
randomParam width (TF.Shape shape) =
    (`TF.mul` stddev) <$> TF.truncatedNormal (TF.vector shape)
    where stddev = TF.scalar (1 / sqrt (fromIntegral width))

-- TODO model
createModel :: TF.Build Model
createModel = do
    images <- TF.placeholder [-1, numPixels]
    -- Hidden layer.
    let numUnits = 500
    hiddenWeights <- TF.initializedVariable =<< randomParam numPixels [numPixels, numUnits]
    hiddenBiases <- TF.zeroInitializedVariable [numUnits]
    let hiddenZ = (images `TF.matMul` TF.readValue hiddenWeights) `TF.add` TF.readValue hiddenBiases
    let hidden = TF.relu hiddenZ
    -- Logits.
    logitWeights <- TF.initializedVariable =<< randomParam numUnits [numUnits, numLabels]
    logitBiases <- TF.zeroInitializedVariable [numLabels]
    let logits = (hidden `TF.matMul` TF.readValue logitWeights) `TF.add` TF.readValue logitBiases
    predict <- TF.render @TF.Build @LabelType $ TF.argMax (TF.softmax logits) (TF.scalar (1 :: LabelType))
    -- Create training action.
    labels <- TF.placeholder [-1]
    let labelVecs = TF.oneHot labels (fromIntegral numLabels) 1 0
    let loss = TF.reduceMean $ fst $ TF.softmaxCrossEntropyWithLogits logits labelVecs
    let params = [hiddenWeights, hiddenBiases, logitWeights, logitBiases]
    trainStep <- TF.minimizeWith TF.adam loss params

    let correctPredictions = TF.equal predict labels
    errorRateTensor <- TF.render $ 1 - TF.reduceMean (TF.cast correctPredictions)

    return Model
        { train = \imFeed lFeed -> TF.runWithFeeds_ 
            [ TF.feed images imFeed
            , TF.feed labels lFeed
            ] trainStep
        , infer = \imFeed -> TF.runWithFeeds 
            [ TF.feed images imFeed
            ] predict
        , errorRate = \imFeed lFeed -> TF.unScalar <$> TF.runWithFeeds
            [ TF.feed images imFeed
            , TF.feed labels lFeed
            ] errorRateTensor
        }

main :: IO ()
main = TF.runSession $ do
    trainingImages <- liftIO (readMNISTSamples =<< trainingImageData)
    trainingLabels <- liftIO (readMNISTLabels =<< trainingLabelData)
    testImages <- liftIO (readMNISTSamples =<< testImageData)
    testLabels <- liftIO (readMNISTLabels =<< testLabelData)

    let encodeImageBatch xs = TF.encodeTensorData [genericLength xs, numPixels]
                                                  (fromIntegral <$> mconcat xs)
    let encodeLabelBatch xs = TF.encodeTensorData [genericLength xs]
                                                  (fromIntegral <$> V.fromList xs)
    let selectBatch i xs = take batchSize $ drop (i * batchSize) (cycle xs)
    let dataSize = 600
    -- let dataSize = length trainingLabels

    model <- TF.build createModel
    forM_ ([1..epochs] :: [Int]) $ \epoch -> do
        liftIO $ printf "\nepoch: %d/%d\n" epoch epochs
        -- train
        forM_ ([0..dataSize] :: [Int]) $ \i -> do
            let images = encodeImageBatch (selectBatch i trainingImages)
                labels = encodeLabelBatch (selectBatch i trainingLabels)
            train model images labels
            when (i `mod` 100 == 0) $ do
                err <- errorRate model images labels
                liftIO $ printf "training: %d/%d, error: %.2f%%\n" i dataSize (err*100)
        -- test
        testErr <- errorRate model (encodeImageBatch testImages) (encodeLabelBatch testLabels)
        liftIO $ printf "\ntest error: %.2f%%\n" (testErr*100)
    -- TODO save model

